#!/bin/bash
set -ex

MYSQL_HOST=$(kubectl get svc vtgate-zone1 -o json | jq -r ".spec.clusterIP")

function prepare() {

sysbench ./sysbench/src/lua/oltp_read_only.lua \
  --threads=16 \
  --mysql-host=$MYSQL_HOST \
  --mysql-port=3306 \
  --mysql-db=sbtest \
  --db-driver=mysql \
  --db-debug=on \
  --tables=2 \
  --table-size=10000 \
  prepare

}

function run() {

sysbench ./sysbench/src/lua/oltp_read_only.lua \
  --threads=16 \
  --mysql-host=$MYSQL_HOST \
  --mysql-port=3306 \
  --mysql-db=sbtest \
  --db-driver=mysql \
  --db-debug=on \
  --debug \
  --tables=2 \
  --table-size=10000 \
  run


}

function main() {

 prepare
# sleep 1
 run 

}

main


