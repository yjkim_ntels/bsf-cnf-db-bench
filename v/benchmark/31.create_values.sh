#!/bin/bash 


SCHEMA=""
VSCHEMA=""

for i in $(seq 1 $1)
do 
#echo $i
SCHEMA="${SCHEMA} CREATE TABLE sbtest$i(id INTEGER NOT NULL AUTO_INCREMENT,k INTEGER DEFAULT '0' NOT NULL,c CHAR(120) DEFAULT '' NOT NULL, pad CHAR(60) DEFAULT '' NOT NULL, PRIMARY KEY (id) ) ENGINE = innodb; \n"
#VSCHEMA="${VSCHEMA} "sbtest$i": {"column_vindexes": [{"column": "id","name": "hash"}],"columns": [{"name": "c","type": "CHAR"},{"name": "pad","type": "CHAR"}]} '
VSCHEMA="${VSCHEMA} \"sbtest$i\": {\"column_vindexes\": [{\"column\": \"id\",\"name\": \"hash\"}],\"columns\": [{\"name\": \"c\",\"type\": \"CHAR\"},{\"name\": \"pad\",\"type\": \"CHAR\"}]},"
done 

cat > .values-bench.yml << END_OF_COMMAND
topology:
  cells:
    - name: "zone1"
      etcd:
        replicas: 1
      vtctld:
        replicas: 1
      vtgate:
        replicas: 1
      mysqlProtocol:
        enabled: true
        authType: "none"
      keyspaces:
        - name: "sbtest"
          shards:
            - name: "0"
              tablets:
                - type: "replica"
                  vttablet:
                    replicas: 2
          schema:
            initial: |-
              ${SCHEMA}
          vschema:
            initial: |-
              {
                "sharded": true,
                "vindexes": {
                  "hash": {
                    "type": "hash"
                  }
                },
                "tables": {
                  ${VSCHEMA}
                }
              }

etcd:
  replicas: 1
  resources:

vtctld:
  serviceType: "NodePort"
  resources:

vtgate:
  serviceType: "NodePort"
  resources:

vttablet:
  mysqlSize: "test"
  resources:
  mysqlResources:
  extraMyCnf: "benchmark-my.cnf"

vtworker:
  resources:

pmm:
  enabled: false

orchestrator:
  enabled: false
END_OF_COMMAND

