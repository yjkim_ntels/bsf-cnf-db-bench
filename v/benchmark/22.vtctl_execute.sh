#!/bin/bash 

HOST=$(kubectl get svc -n vitess vtctld -o json | jq -r ".spec.clusterIP")
PORT=15999

vtctlclient -server $HOST:$PORT "$@" 
