package main

import (
	"bytes"
	"database/sql"
	"fmt"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

/* test #1 */

var (
	connection  = "root@tcp(10.233.56.187:3306)/t"
	threads     = 2
	insertRow   = "(RANDOM_BYTES(512),NOW())"
	createTable = "CREATE TABLE t (id INT NOT NULL PRIMARY KEY auto_increment, a VARBINARY(512) , n DATETIME NOT NULL NOT NULL )"
        w = new(sync.WaitGroup)
)

func main() {

	db, err := sql.Open("mysql", connection+":80-")
	if err != nil {
		fmt.Printf("Could not connect to MySQL: %s.", err)
	}

	buf := new(bytes.Buffer)
	for i := 0; i < 10; i++ {
		buf.WriteString(insertRow)
		buf.WriteString(",")
	}
	batchedInsert := fmt.Sprintf("INSERT INTO t (a,n) VALUES %s %s;", buf.String(), insertRow)

//	db.Exec("DROP TABLE IF EXISTS t")
//	db.Exec(createTable)
	w.Add(1)

	go monitor(db)

	for i := 0; i < threads; i++ {
		go insertOnLoop(db, batchedInsert)
	}

	w.Wait()

}

func insertOnLoop(db *sql.DB, sql string) {
	for {
		_, err := db.Exec(sql)
		if err != nil {
			fmt.Printf("error: %s\n", err)
		} else {
			//              fmt.Printf(".")
		}
	}
}

func monitor(db *sql.DB) {

	prevcount, newcount := 0, 0
	var fake string
	fmt.Printf("Time,Rows\n")

	for i := 0; i <= 3600; i++ {

		rows, err := db.Query("SHOW GLOBAL STATUS LIKE 'Innodb_rows_inserted'")
		if err != nil {
			fmt.Printf("could not dump count: %s", err)
		}

		for rows.Next() {
			err = rows.Scan(&fake, &newcount)
			if err != nil {
				fmt.Printf("could not dump count: %s", err)
			}
		}

		if prevcount > 0 {
			fmt.Printf("%d,%d\n", i, (newcount - prevcount))
		}

		prevcount = newcount
		rows.Close()
		time.Sleep(time.Second)
	}
	w.Done()

}

