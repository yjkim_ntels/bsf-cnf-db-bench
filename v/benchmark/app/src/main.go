package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func insert() {
	db, err := sql.Open("mysql", "root@tcp(10.233.33.37:3306)/t")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// INSERT 문 실행
	result, err := db.Exec("INSERT INTO t(n,a) VALUES (1,1)")
	if err != nil {
		log.Fatal(err)
	}

	//https://golang.org/pkg/database/sql/#Result
	n, err := result.RowsAffected()
	if n == 1 {
		fmt.Println("1 row inserted.")
	}
}

func get() {

	db, err := sql.Open("mysql", "root@tcp(10.233.33.37:3306)/t")
	var id int
	var n string
	var a string
	rows, err := db.Query("SELECT * FROM t where id = 1")

	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close() 

	for rows.Next() {
		err := rows.Scan(&id, &n, &a)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(id, n, a)
	}
}

func main() {
	//insert()
	get()
}

