#!/bin/bash

MYSQL_HOST=$(kubectl get svc vtgate-zone1 -o json | jq -r ".spec.clusterIP")

function execute_prepare() {
date

TEST=./sysbench/src/lua/oltp_read_only.lua
#TEST=./sysbench/src/lua/oltp_insert.lua

sysbench $TEST \
  --config-file=.config \
  --threads=16 \
  --time=60 \
  --report-interval=10 \
  --debug=off \
  prepare

date

}

function main() {
 execute_prepare
}


main
