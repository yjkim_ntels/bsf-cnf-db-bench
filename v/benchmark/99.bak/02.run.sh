#!/bin/bash
set -ex

MYSQL_HOST=$(kubectl get svc vtgate-zone1 -o json | jq -r ".spec.clusterIP")

date

#TEST=./sysbench/src/lua/oltp_read_only.lua
TEST=./sysbench/src/lua/oltp_insert.lua

sysbench $TEST \
  --threads=100 \
  --mysql-host=$MYSQL_HOST  \
  --mysql-db=sbtest \
  --mysql-port=3306 \
  --tables=5 \
  --table-size=200000 \
  run

date
