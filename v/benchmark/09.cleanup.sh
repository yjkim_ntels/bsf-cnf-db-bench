#!/bin/bash

MYSQL_HOST=$(kubectl get svc vtgate-zone1 -o json | jq -r ".spec.clusterIP")


TABLES=$(mysql -h $MYSQL_HOST -P3306 -Nse "show tables")

for TABLE in $TABLES
do
 mysql -h $MYSQL_HOST -P3306 -Nse "drop table $TABLE"
done
