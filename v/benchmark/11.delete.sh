#!/bin/bash 

helm delete --purge vitess

kubectl delete po $(kubectl get po | grep zone1 | awk '{print $1}')  --grace-period=0 --force

kubectl delete pvc -n vitess --all


