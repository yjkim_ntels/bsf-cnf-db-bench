#!/bin/bash 

helm fetch pingcap/tidb-operator

mkdir tidb-operator

tar -xvf tidb-operator-v*.tgz -C ./

rm -rf tidb-operator-v*.tgz

sed -i "s/defaultStorageClassName: local-storage/defaultStorageClassName: tidb-operator-block/"    tidb-operator/values.yaml
