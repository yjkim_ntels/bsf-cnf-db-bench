#!/bin/bash

if [[ $# -ne 4 ]]
then
  echo "usage: run.sh [test name] [prepare/run/help] [threads]"
  echo "       e.g. run.sh point_select prepare 16 cnt:10"
  exit 1
fi

sysbench --config-file=config --threads=$3 oltp_$1 --tables=$3 --table_size=$4 $2 > $1_$2_$3.log 2>&1
