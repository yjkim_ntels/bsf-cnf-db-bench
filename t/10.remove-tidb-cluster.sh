#!/bin/bash

helm delete --purge tidb-cluster 
kubectl delete pvc $(kubectl get pvc | grep tidb-cluster | awk '{print $1}')
kubectl delete pv -n tidb $(kubectl get pv | grep tidb | awk '{print $1}')
