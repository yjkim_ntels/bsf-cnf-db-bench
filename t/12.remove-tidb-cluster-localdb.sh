#!/bin/bash

helm delete --purge tidb-cluster 
kubectl delete pvc $(kubectl get pvc | grep tidb-cluster | awk '{print $1}')
kubectl delete pv -n tidb $(kubectl get pv | grep tidb | awk '{print $1}')

ssh k8s-master rm -rf /data/storage-data/tikv-0/*
ssh k8s-master rm -rf /data/storage-data/tikv-1/*
ssh k8s-master rm -rf /data/storage-data/tikv-2/*
ssh k8s-master rm -rf /data/storage-data/tikv-3/*
ssh k8s-master rm -rf /data/storage-data/tikv-4/*
ssh k8s-master rm -rf /data/storage-data/tikv-5/*

ssh k8s-node1 rm -rf /data/storage-data/tikv-0/*
ssh k8s-node1 rm -rf /data/storage-data/tikv-1/*
ssh k8s-node1 rm -rf /data/storage-data/tikv-2/*
ssh k8s-node1 rm -rf /data/storage-data/tikv-3/*
ssh k8s-node1 rm -rf /data/storage-data/tikv-4/*
ssh k8s-node1 rm -rf /data/storage-data/tikv-5/*

ssh k8s-node2 rm -rf /data/storage-data/tikv-0/*
ssh k8s-node2 rm -rf /data/storage-data/tikv-1/*
ssh k8s-node2 rm -rf /data/storage-data/tikv-2/*
ssh k8s-node2 rm -rf /data/storage-data/tikv-3/*
ssh k8s-node2 rm -rf /data/storage-data/tikv-4/*
ssh k8s-node2 rm -rf /data/storage-data/tikv-5/*

