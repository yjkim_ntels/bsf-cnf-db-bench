#!/bin/bash 

function create_pv(){


cat << EOF | kubectl apply -f -
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: tikv-tidb-cluster-tikv-0
spec:
  capacity:
    storage: 10Gi
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Delete
  storageClassName: local-storage
  local:
    path: /data/storage-data/tikv-0
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - k8s-master
          - k8s-node1
          - k8s-node2
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: tikv-tidb-cluster-tikv-1
spec:
  capacity:
    storage: 10Gi
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Delete
  storageClassName: local-storage
  local:
    path: /data/storage-data/tikv-1
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - k8s-master
          - k8s-node1
          - k8s-node2
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: tikv-tidb-cluster-tikv-2
spec:
  capacity:
    storage: 10Gi
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Delete
  storageClassName: local-storage
  local:
    path: /data/storage-data/tikv-2
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - k8s-master
          - k8s-node1
          - k8s-node2
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: tikv-tidb-cluster-tikv-3
spec:
  capacity:
    storage: 10Gi
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Delete
  storageClassName: local-storage
  local:
    path: /data/storage-data/tikv-3
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - k8s-master
          - k8s-node1
          - k8s-node2
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: tikv-tidb-cluster-tikv-4
spec:
  capacity:
    storage: 10Gi
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Delete
  storageClassName: local-storage
  local:
    path: /data/storage-data/tikv-4
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - k8s-master
          - k8s-node1
          - k8s-node2
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: tikv-tidb-cluster-tikv-5
spec:
  capacity:
    storage: 10Gi
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Delete
  storageClassName: local-storage
  local:
    path: /data/storage-data/tikv-5
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - k8s-master
          - k8s-node1
          - k8s-node2
---
EOF


}

function install_tidb_cluster() {

helm install ./tidb-cluster \
  --name=tidb-cluster \
  --namespace=tidb

}


create_pv
install_tidb_cluster
