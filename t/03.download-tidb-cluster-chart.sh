#!/bin/bash 

helm search pingcap/tidb-cluster 
helm fetch pingcap/tidb-cluster

mkdir tidb-cluster

tar xvf tidb-cluster-v*.tgz -C ./

rm -rf tidb-cluster-v*.tgz
