package main

import (
	"database/sql"
	"fmt"
	"runtime/debug"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	debug.SetGCPercent(-1) // disable GC

	sqlDB, err := sql.Open("mysql", "root@tcp(10.233.63.189:4000)/test")
	if err != nil {
		panic(err)
	}
	defer sqlDB.Close()

	sqlDB.SetMaxOpenConns(10000)

	objectsInHEAP(func() string { return insert(10000, sqlDB) })
	objectsInHEAP(func() string { return read(sqlDB) })
	objectsInHEAP(func() string { return update(10000, sqlDB) })

}

func read(db *sql.DB) string {
	rows, err := db.Query("SELECT n FROM t")
	if err != nil {
		panic(err)
	}

	count := 0
	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			panic(err)
		}
		count++
		_ = name
	}

	return "records read " + strconv.Itoa(count)
}

func objectsInHEAP(fn func() string) {
	now := time.Now()
	prefix := fn()
	took := time.Since(now)
	fmt.Println(prefix, " time", took)
}

func insert(num int, db *sql.DB) string {

	if _, err := db.Exec(`DROP TABLE IF EXISTS t`); err != nil {
		panic(err)
	}
	if _, err := db.Exec(`CREATE TABLE t (
		id int NOT NULL AUTO_INCREMENT,
		n varchar(255),
		a int,
		PRIMARY KEY (id)
	)`); err != nil {
		panic(err)
	}

	for i := 0; i < num; i++ {
	  _, err := db.Exec(`INSERT INTO t(n) VALUES("` + strconv.Itoa(i) + `")`)
          if err != nil {
           panic(err)
          }
	}

	return "records insert " + strconv.Itoa(num)

}

func update(num int, db *sql.DB) string {

	stmt, err := db.Prepare("UPDATE t SET n=n+1 WHERE id=?")
	if err != nil {
         panic(err)
	}

	for i := 1; i < num+1; i++ {
		_, err := stmt.Exec(i)
		if err != nil {
			panic(err)
		}
	}

	return "records update " + strconv.Itoa(num)
}

