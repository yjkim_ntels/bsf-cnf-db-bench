#!/bin/bash 

helm install ./tidb-operator \
  --name=tidb-operator \
  --namespace=tidb \
  -f ./tidb-operator/values.yaml \
  --debug
